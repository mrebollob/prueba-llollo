package com.mrb.llollo.di;


import android.app.Application;

import com.mrb.llollo.di.module.RootModule;

import dagger.ObjectGraph;


public class AppController extends Application {

    private ObjectGraph objectGraph;

    @Override
    public void onCreate() {
        super.onCreate();
        injectDependencies();
    }

    private void injectDependencies() {
        objectGraph = ObjectGraph.create(new RootModule(this));
        objectGraph.inject(this);
    }

    public void inject(Object object) {
        objectGraph.inject(object);
    }
}

package com.mrb.llollo.di.module;


import android.content.Context;

import com.mrb.llollo.di.AppController;
import com.mrb.llollo.mvp.presenter.ValetDetailsPresenterImp;
import com.mrb.llollo.mvp.presenter.ValetsListPresenterImp;
import com.mrb.llollo.ui.activity.MainActivity;
import com.mrb.llollo.ui.activity.ValetDetailsActivity;
import com.mrb.llollo.ui.fragment.ValetDetailsFragment;
import com.mrb.llollo.ui.fragment.ValetsListFragment;
import com.squareup.otto.Bus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(
        includes = {
                RepositoryModule.class,
                PresenterModule.class
        },
        injects = {
                AppController.class,
                MainActivity.class,
                ValetDetailsActivity.class,
                ValetsListFragment.class,
                ValetsListPresenterImp.class,
                ValetDetailsFragment.class,
                ValetDetailsPresenterImp.class
        },
        library = true
)
public class RootModule {

    private final Context context;

    public RootModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    public Context provideApplicationContext() {
        return context;
    }

    @Provides
    @Singleton
    public Bus provideBusEvent() {
        return new Bus();
    }
}

package com.mrb.llollo.di.module;


import com.mrb.llollo.api.mapper.Mapper;
import com.mrb.llollo.api.mapper.ValetsMapper;
import com.mrb.llollo.api.retrofit.RetrofitValetsApi;
import com.mrb.llollo.api.valets.ValetsApi;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(
        complete = false,
        library = true
)
public class RepositoryModule {

    @Provides
    @Named("valets")
    @Singleton
    public Mapper providesValetsMapper() {
        return new ValetsMapper();
    }

    @Provides
    @Singleton
    public ValetsApi providesValetsApi(
            @Named("valets")
            final Mapper mapper) {
        return new RetrofitValetsApi(mapper);
    }
}

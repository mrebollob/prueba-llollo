package com.mrb.llollo.di.module;

import android.content.Context;

import com.mrb.llollo.api.valets.ValetsApi;
import com.mrb.llollo.mvp.presenter.ValetDetailsPresenter;
import com.mrb.llollo.mvp.presenter.ValetDetailsPresenterImp;
import com.mrb.llollo.mvp.presenter.ValetsListPresenter;
import com.mrb.llollo.mvp.presenter.ValetsListPresenterImp;
import com.squareup.otto.Bus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(
        complete = false,
        library = true
)
public class PresenterModule {

    @Provides
    @Singleton
    ValetsListPresenter providesValetsListPresenter(Context appContext, ValetsApi valetsApi,
                                                    Bus eventBus) {
        return new ValetsListPresenterImp(appContext, valetsApi, eventBus);
    }

    @Provides
    @Singleton
    ValetDetailsPresenter providesMovieDetailsPresenter() {
        return new ValetDetailsPresenterImp();
    }
}

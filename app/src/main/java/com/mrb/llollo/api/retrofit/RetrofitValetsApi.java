package com.mrb.llollo.api.retrofit;

import android.location.Location;

import com.mrb.llollo.api.mapper.Mapper;
import com.mrb.llollo.api.valets.ValetsApi;
import com.mrb.llollo.api.valets.model.ValetsResult;
import com.mrb.llollo.mvp.model.Valet;

import java.util.List;

import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class RetrofitValetsApi implements ValetsApi {

    private final static String TAG = RetrofitValetsApi.class.getSimpleName();

    private ValetsService valetsService;
    private final Mapper valetsApiMapper;

    public RetrofitValetsApi(Mapper mapper) {
        this.valetsApiMapper = mapper;
        initRestAdapter();
    }

    private void initRestAdapter() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .build();

        valetsService = restAdapter.create(ValetsService.class);
    }

    @Override
    public void getValets(final Location location, final Callback callback) {

        UserLocation UserLocation = new UserLocation(String.valueOf(location.getLatitude()),
                String.valueOf(location.getLongitude()));

        valetsService.getValets(UserLocation,
                new retrofit.Callback<ValetsResult>() {
                    @Override
                    public void success(ValetsResult valetsResult, Response response) {
                        callback.onFinish((List<Valet>) valetsApiMapper.map(valetsResult));
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        callback.onError(error.getMessage());
                    }
                });
    }
}

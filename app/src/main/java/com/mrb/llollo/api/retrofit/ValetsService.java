package com.mrb.llollo.api.retrofit;

import com.mrb.llollo.api.valets.model.ValetsResult;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Headers;
import retrofit.http.POST;


public interface ValetsService {
    @Headers({
            "Accept: */*"
    })
    @POST("/get_valet_list")
    void getValets(@Body UserLocation userLocation, Callback<ValetsResult> callback);
}
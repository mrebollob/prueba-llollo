package com.mrb.llollo.api.valets.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ValetsResult {

    @SerializedName("valets")
    @Expose
    private List<ValetResponse> valets = new ArrayList<ValetResponse>();

    public List<ValetResponse> getResults() {
        return valets;
    }

    public void setResults(List<ValetResponse> results) {
        this.valets = results;
    }
}

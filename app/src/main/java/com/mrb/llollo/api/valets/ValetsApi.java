package com.mrb.llollo.api.valets;

import android.location.Location;

import com.mrb.llollo.mvp.model.Valet;

import java.util.List;


public interface ValetsApi {

    static final String BASE_URL = "http://llollodevs1234.herokuapp.com";

    void getValets(Location location, Callback callback);

    public interface Callback {

        void onFinish(List<Valet> valet);

        void onError(String errorMessage);
    }
}

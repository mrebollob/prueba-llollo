package com.mrb.llollo.api.mapper;

import com.mrb.llollo.api.valets.model.ValetResponse;
import com.mrb.llollo.api.valets.model.ValetsResult;
import com.mrb.llollo.mvp.model.Valet;

import java.util.ArrayList;
import java.util.List;


public class ValetsMapper implements Mapper<ValetsResult, List<Valet>> {

    @Override
    public List<Valet> map(ValetsResult valetsResult) {
        List<Valet> valets = new ArrayList<Valet>();
        for (ValetResponse valetResponse : valetsResult.getResults()) {
            valets.add(mapValet(valetResponse));
        }
        return valets;
    }

    private Valet mapValet(ValetResponse valetResponse) {
        Valet valet = new Valet();
        valet.setName(valetResponse.getName());
        valet.setPhoneNumber(valetResponse.getPhoneNumber());
        valet.setEmail(valetResponse.getEmail());
        valet.setImageURL(valetResponse.getImageURL());

        return valet;
    }
}

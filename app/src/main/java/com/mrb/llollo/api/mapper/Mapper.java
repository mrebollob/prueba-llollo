package com.mrb.llollo.api.mapper;

public interface Mapper<TFrom, TTo> {
    TTo map(TFrom from);
}

package com.mrb.llollo.api.retrofit;


public class UserLocation {

    private String latitude;
    private String longitude;

    public UserLocation(String latitude, String longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
}

package com.mrb.llollo.bus.event;

import com.mrb.llollo.mvp.model.Valet;


public class ValetSelectedEvent {

    private final Valet valet;

    public ValetSelectedEvent(Valet valet) {
        this.valet = valet;
    }

    public Valet getValet() {
        return valet;
    }
}

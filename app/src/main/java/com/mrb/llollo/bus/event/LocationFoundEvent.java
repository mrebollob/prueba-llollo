package com.mrb.llollo.bus.event;

import android.location.Location;


public class LocationFoundEvent {

    private final Location location;

    public LocationFoundEvent(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }
}

package com.mrb.llollo.ui.activity;

import android.os.Bundle;

import com.mrb.llollo.R;
import com.mrb.llollo.mvp.model.Valet;
import com.mrb.llollo.ui.fragment.ValetDetailsFragment;
import com.mrb.llollo.utils.Constants;

public class ValetDetailsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_valet_details);
        initValetDetailsFragment(getValetFromBundle());
    }

    private Valet getValetFromBundle() {
        return getIntent().getExtras().getParcelable(Constants.KEY_VALET_DETAILS);
    }

    private void initValetDetailsFragment(Valet valet) {
        ValetDetailsFragment valetDetailsFragment = ValetDetailsFragment.newInstance(valet);
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.frm_valet_details, valetDetailsFragment)
                .disallowAddToBackStack()
                .commit();
    }
}
package com.mrb.llollo.ui.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.mrb.llollo.di.AppController;

public class BaseActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        injectDependencies();
    }

    private void injectDependencies() {
        ((AppController) getApplicationContext()).inject(this);
    }
}

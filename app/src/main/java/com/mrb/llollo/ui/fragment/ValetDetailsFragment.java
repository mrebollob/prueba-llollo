package com.mrb.llollo.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mrb.llollo.R;
import com.mrb.llollo.mvp.model.Valet;
import com.mrb.llollo.mvp.presenter.ValetDetailsPresenter;
import com.mrb.llollo.mvp.view.ValetDetailsView;
import com.mrb.llollo.utils.Constants;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.InjectView;


public class ValetDetailsFragment extends BaseFragment implements ValetDetailsView {

    @Inject
    protected ValetDetailsPresenter valetDetails;

    @InjectView(R.id.iv_image)
    protected ImageView ivImage;
    @InjectView(R.id.tv_name)
    protected TextView tvName;
    @InjectView(R.id.tv_phone_number)
    protected TextView tvPhoneNumber;
    @InjectView(R.id.tv_email)
    protected TextView tvEmail;


    public static ValetDetailsFragment newInstance(Valet valet) {
        ValetDetailsFragment valetDetailsFragment = new ValetDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(Constants.KEY_VALET_DETAILS, valet);
        valetDetailsFragment.setArguments(bundle);
        return valetDetailsFragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initPresenter();
        valetDetails.onValetReceive(getValet());
    }

    private void initPresenter() {
        valetDetails.setView(this);
        valetDetails.initialize();
    }

    private Valet getValet() {
        return getArguments().getParcelable(Constants.KEY_VALET_DETAILS);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_valet_details, container, false);
    }

    @Override
    public void renderValet(Valet valet) {
        Picasso.with(getActivity().getApplicationContext())
                .load(valet.getImageURL())
                .into(ivImage);
        tvName.setText(valet.getName());
        tvPhoneNumber.setText(valet.getPhoneNumber());
        tvEmail.setText(valet.getEmail());
    }
}

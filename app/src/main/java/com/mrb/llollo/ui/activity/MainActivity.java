package com.mrb.llollo.ui.activity;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.mrb.llollo.R;
import com.mrb.llollo.bus.event.LocationFoundEvent;
import com.mrb.llollo.bus.event.ValetSelectedEvent;
import com.mrb.llollo.mvp.model.Valet;
import com.mrb.llollo.utils.Constants;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import javax.inject.Inject;


public class MainActivity extends BaseActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = MainActivity.class.getSimpleName();
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;

    private GoogleApiClient googleApiClient;

    @Inject
    protected Bus bus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (checkPlayServices()) {
            buildGoogleApiClient();
        } else {
            Toast.makeText(getApplicationContext(),
                    "This device is not supported.", Toast.LENGTH_LONG)
                    .show();
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerBus();
        checkPlayServices();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unRegisterBus();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (googleApiClient != null) {
            googleApiClient.connect();
        }
    }

    private void registerBus() {
        bus.register(this);
    }

    private void unRegisterBus() {
        bus.unregister(this);
    }

    @Subscribe
    public void onItemSelectedEvent(ValetSelectedEvent event) {
        launchValetDetailsActivity(event.getValet());
    }

    private void launchValetDetailsActivity(Valet valet) {
        Intent detailsActivityIntent = new Intent(this, ValetDetailsActivity.class);
        detailsActivityIntent.putExtra(Constants.KEY_VALET_DETAILS, valet);
        detailsActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(detailsActivityIntent);
    }

    protected synchronized void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                finish();
            }
            return false;
        }
        return true;
    }

    private void setLocation() {
        Location location = LocationServices.FusedLocationApi
                .getLastLocation(googleApiClient);

        bus.post(new LocationFoundEvent(location));
    }

    @Override
    public void onConnected(Bundle bundle) {
        setLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {
        googleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed: "
                + connectionResult.getErrorCode());
    }
}

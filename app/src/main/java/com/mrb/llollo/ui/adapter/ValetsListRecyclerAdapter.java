package com.mrb.llollo.ui.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mrb.llollo.R;
import com.mrb.llollo.mvp.model.Valet;

import java.util.Collections;
import java.util.List;

import butterknife.InjectView;

public class ValetsListRecyclerAdapter extends RecyclerView.Adapter<ValetsListRecyclerAdapter.ViewHolder> {

    private List<Valet> valets = Collections.emptyList();
    private final Context context;

    public ValetsListRecyclerAdapter(Context context) {
        this.context = context;
    }

    public void updateItems(List<Valet> valets) {
        this.valets = valets;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View modelView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.valet_list_item,
                viewGroup, false);
        return new ViewHolder(modelView);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        Valet valet = valets.get(i);
        renderValetView(valet, viewHolder);
    }

    @Override
    public int getItemCount() {
        return valets.size();
    }

    private void renderValetView(Valet valet, ViewHolder viewHolder) {
        viewHolder.valetName.setText(valet.getName());
    }

    public class ViewHolder extends BaseRecyclerViewHolder {

        @InjectView(R.id.tv_valet_name)
        protected TextView valetName;

        public ViewHolder(View view) {
            super(view);
        }
    }
}

package com.mrb.llollo.ui.fragment;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mrb.llollo.R;
import com.mrb.llollo.bus.event.LocationFoundEvent;
import com.mrb.llollo.mvp.model.Valet;
import com.mrb.llollo.mvp.presenter.ValetsListPresenter;
import com.mrb.llollo.mvp.view.ValetsListView;
import com.mrb.llollo.ui.adapter.ValetsListRecyclerAdapter;
import com.mrb.llollo.ui.view.ClickRecyclerView;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.List;

import javax.inject.Inject;

import butterknife.InjectView;


public class ValetsListFragment extends BaseFragment implements ValetsListView {

    @Inject
    protected Bus bus;

    @Inject
    protected ValetsListPresenter valetsListPresenter;

    @InjectView(R.id.rv_valets_list)
    protected ClickRecyclerView valetsListView;
    @InjectView(R.id.pb_list)
    protected ProgressBar pbList;

    private ValetsListRecyclerAdapter valetsListAdapter;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initValetsRecyclerView();
        initPresenter();
    }

    private void initValetsRecyclerView() {
        valetsListView.setLayoutManager(new LinearLayoutManager(getActivity()
                .getApplicationContext()));
        valetsListView.setItemAnimator(new DefaultItemAnimator());
        valetsListAdapter = new ValetsListRecyclerAdapter(getActivity().getApplicationContext());
        valetsListView.setAdapter(valetsListAdapter);
        valetsListView.setOnItemClickListener(new ClickRecyclerView.OnItemClickListener() {
            @Override
            public void onItemClick(RecyclerView parent, View view, int position, long id) {
                valetsListPresenter.onItemSelected(position);
            }
        });
    }

    private void initPresenter() {
        valetsListPresenter.setView(this);
        valetsListPresenter.initialize();
    }

    @Subscribe
    public void onLocationFoundEvent(LocationFoundEvent event) {
        valetsListPresenter.onLocationFound(event.getLocation());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_valets_list, container, false);
    }

    @Override
    public void showLoading() {
        pbList.setVisibility(View.VISIBLE);
    }

    @Override
    public void showError(String error) {
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void hideLoading() {
        pbList.setVisibility(View.GONE);
    }

    @Override
    public void renderValets(List<Valet> valets) {
        valetsListAdapter.updateItems(valets);
    }

    @Override
    public void onResume() {
        super.onResume();
        registerBus();
    }

    @Override
    public void onPause() {
        super.onPause();
        unRegisterBus();
    }

    private void registerBus() {
        bus.register(this);
    }

    private void unRegisterBus() {
        bus.unregister(this);
    }
}
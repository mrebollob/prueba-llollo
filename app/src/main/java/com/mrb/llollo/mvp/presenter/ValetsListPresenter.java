package com.mrb.llollo.mvp.presenter;


import android.location.Location;

import com.mrb.llollo.mvp.view.ValetsListView;

public abstract class ValetsListPresenter extends Presenter<ValetsListView> {

    public abstract void onLocationFound(Location location);

    public abstract void onItemSelected(int position);
}

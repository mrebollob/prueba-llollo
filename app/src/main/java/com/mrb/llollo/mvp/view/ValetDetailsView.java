package com.mrb.llollo.mvp.view;


import com.mrb.llollo.mvp.model.Valet;

public interface ValetDetailsView extends View {

    void renderValet(final Valet valet);
}

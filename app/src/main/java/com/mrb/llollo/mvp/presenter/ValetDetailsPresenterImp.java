package com.mrb.llollo.mvp.presenter;

import com.mrb.llollo.mvp.model.Valet;


public class ValetDetailsPresenterImp extends ValetDetailsPresenter {

    public ValetDetailsPresenterImp() {
    }

    @Override
    public void onValetReceive(Valet valet) {
        view.renderValet(valet);
    }

    @Override
    public void initialize() {
    }
}

package com.mrb.llollo.mvp.view;

import com.mrb.llollo.mvp.model.Valet;

import java.util.List;


public interface ValetsListView extends View {

    void showLoading();

    void showError(String error);

    void hideLoading();

    void renderValets(final List<Valet> valets);
}

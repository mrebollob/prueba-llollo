package com.mrb.llollo.mvp.presenter;


import android.content.Context;
import android.location.Location;
import android.util.Log;

import com.mrb.llollo.api.valets.ValetsApi;
import com.mrb.llollo.bus.event.ValetSelectedEvent;
import com.mrb.llollo.mvp.model.Valet;
import com.mrb.llollo.utils.Utils;
import com.squareup.otto.Bus;

import java.util.Collections;
import java.util.List;

public class ValetsListPresenterImp extends ValetsListPresenter {

    private final static String TAG = ValetsListPresenterImp.class.getSimpleName();

    private ValetsApi valetsApi;
    private List<Valet> valets = Collections.emptyList();
    private final Bus bus;
    private Context appContext;

    public ValetsListPresenterImp(Context appContext, ValetsApi valetsApi, Bus bus) {
        this.valetsApi = valetsApi;
        this.bus = bus;
        this.appContext = appContext;
    }

    @Override
    public void initialize() {
    }

    @Override
    public void onLocationFound(Location location) {
        loadValets(location);
    }

    @Override
    public void onItemSelected(int position) {
        bus.post(new ValetSelectedEvent(valets.get(position)));
    }

    private void loadValets(Location location) {
        if (Utils.isOnline(appContext)) {
            view.showLoading();
            valetsApi.getValets(location, new ValetsApi.Callback() {
                @Override
                public void onFinish(List<Valet> valets) {
                    setValets(valets);
                    showValets(valets);
                    view.hideLoading();
                }

                @Override
                public void onError(String errorMessage) {
                    Log.i(TAG, "Error: " + errorMessage);
                    view.hideLoading();
                }
            });
        } else {
            view.hideLoading();
            view.showError("Red no disponible");
        }
    }

    private void showValets(List<Valet> valets) {
        view.renderValets(valets);
    }

    private void setValets(List<Valet> valets) {
        this.valets = valets;
    }
}

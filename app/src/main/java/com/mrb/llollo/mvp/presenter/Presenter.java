package com.mrb.llollo.mvp.presenter;

import com.mrb.llollo.mvp.view.View;

public abstract class Presenter<T extends View> {

    protected T view;

    public void setView(T view) {
        this.view = view;
    }

    public abstract void initialize();

}

package com.mrb.llollo.mvp.presenter;

import com.mrb.llollo.mvp.model.Valet;
import com.mrb.llollo.mvp.view.ValetDetailsView;


public abstract class ValetDetailsPresenter extends Presenter<ValetDetailsView> {

    public abstract void onValetReceive(Valet valet);
}

package com.mrb.llollo.mvp.model;


import android.os.Parcel;
import android.os.Parcelable;

public class Valet implements Parcelable {

    private String name;
    private String phoneNumber;
    private String email;
    private String imageURL;

    public Valet() {
    }

    protected Valet(Parcel in) {
        name = in.readString();
        phoneNumber = in.readString();
        email = in.readString();
        imageURL = in.readString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(phoneNumber);
        dest.writeString(email);
        dest.writeString(imageURL);
    }

    public static final Parcelable.Creator<Valet> CREATOR = new Parcelable.Creator<Valet>() {
        @Override
        public Valet createFromParcel(Parcel in) {
            return new Valet(in);
        }

        @Override
        public Valet[] newArray(int size) {
            return new Valet[size];
        }
    };
}
